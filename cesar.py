#!/usr/bin/python3
# -*- coding: utf-8 -*-

minuscules = 'abcdefghijklmnopqrstuvwxyz'
majuscules = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def rotation(chaine, x):
    """
     Effectue une rotation de x caractères vers la droite:
     >>> rotation('abcde', 2)
     'cdeab'
    """

def index(c, chaine):
    """
     Trouve l'index de c dans la chaine:
     >>> index('n', 'bonjour')
     2
    """

def chiffre(chaine, x):
    """
     Chiffre une chaîne quelconque
     >>> chiffre('Bonjour les amis!', 3)
     'Erqmrxu ohv dplv!'
    """
    resultat = ''
    return resultat

result = chiffre("salut", 3)